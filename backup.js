const fs = require('fs')
const util = require('util')
const exec = util.promisify(require('child_process').exec)
const outputfile = './backupdata.json'
async function backupDb(key, { username, password, database, host, port }) {
    let outputfilename = `${key}_${database}_${new Date().getTime()}.sql`
    try {
        const { stdout, stderr } = await exec(`PGPASSWORD=${password} pg_dump -U ${username} -h ${host} -p ${port} ${database} > ${outputfilename}`)
        if (stderr)
            throw new Error(`stderr: ${stderr}`)
        console.log(`stdout: ${stdout}`)
    } catch (error) {
        console.log(error)
        return
    }
    return outputfilename
}

function writeBackupUrlsToFile(backupdata) {
    try {
        fs.unlinkSync(outputfile)
    } catch (error) {
        console.log(error)
    }
    fs.writeFileSync(outputfile, JSON.stringify(backupdata), 'utf-8')
}


async function postProcess(key, backupfiles, filename, url, s3upload) {
    // Delete local file
    if (filename && url) {
        backupfiles[key][new Date().getTime()] = { filename, url }
        let backups = backupfiles[key]
        // We will not be storing more than 7 files per db, delete the oldest entry if there are more than 7 backups
        if (Object.keys(backups).length > 7) {
            let backupDates = Object.keys(backups).sort(function (a, b) {
                return parseInt(a) > parseInt(b)
            })
            let backupToDelete = backups[backupDates[backupDates.length - 1]]
            try {

                await s3upload.deleteFromS3(backupToDelete.filename)
                delete backups[backupDates[backupDates.length - 1]]
                backupfiles[key] = backups
            } catch( error ){
                console.log(error)
            }
        }
        writeBackupUrlsToFile(backupfiles)
    }
}

function readBackupFile(){
    if(fs.existsSync(outputfile)){
        let fileData = fs.readFileSync(outputfile, { encoding: 'utf-8'})
        if( fileData)
            return JSON.parse(fileData)
        else return {}
    } else {
        return {}
    }
}

module.exports = async function (dbconfigs, s3upload) {
    let backupfiles = readBackupFile()
    console.log(backupfiles)
    // Get sql dumps of all the databases and store it in dbconfigs
    for (const key in dbconfigs) {
        console.log('Backing up', key)
        let backupfile = await backupDb(key, dbconfigs[key])
        if (!backupfiles[key]) {
            backupfiles[key] = {}
        }
        if (backupfile) {
            console.log('Backup file: ', backupfile)
            //backupfiles[key][`${new Date().getTime()}`] = backupfile
            // Upload the file in s3
            let backupfileData = fs.readFileSync(backupfile, 'utf8')
            let filename, url
            fs.unlinkSync(backupfile)
            try {
                let result = await s3upload.uploadToS3(backupfile, backupfileData)
                filename = result.Key
                url = result.url
                console.log(key, url)
                await postProcess(key, backupfiles, filename, url, s3upload)
            } catch (error) {
                console.log(error)
                return
            }
        }
    }
}