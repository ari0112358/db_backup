/**
 * Application entry point
 */
let run_config = 'dev'
if( process.env.NODE_ENV)
    run_config = process.env.NODE_ENV
else if( process.argv[2] )
    run_config = process.argv[2]

const db_config = require('./db/dbconfigs')(run_config)
const s3upload = require('./s3/s3upload')()

const backup = require('./backup')(db_config.getActiveConfigs(), s3upload)