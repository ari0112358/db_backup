const configs = require('./configs')
/**
 * Sets active configs based on @param run_config
 */
module.exports = function(run_config){
    let active_configs = {}
    if(!configs)
        throw new Error('No database configs found')
    
    for( const key in configs){
        let config = (configs[key])[run_config]
        active_configs[key] = config
    }

    return {
        getActiveConfigs(){
            return active_configs
        }
    }
}