const AWS = require('aws-sdk')
const config = require('./s3Config')

const BUCKET_NAME = 'aphelia-sc-asset'
module.exports = function(){
    AWS.config.update(config)
    
    let bucketConfig = config
    delete bucketConfig.region
    bucketConfig.Bucket = BUCKET_NAME

    let s3Bucket = new AWS.S3(bucketConfig)

    function uploadPromise(Key, Body){
        return new Promise(function(resolve, reject){
            s3Bucket.upload({
                Bucket: BUCKET_NAME,
                Key,
                Body,
                ACL: 'public-read'
            }, function(error, data){
                if(error){
                    reject(error)
                }
                resolve(data)
            })
        })
    }

    return {
        uploadToS3(fileName, fileStream){
            return new Promise(function(resolve, reject){
                uploadPromise(fileName, fileStream).then(function({ Key }){
                    let url = s3Bucket.getSignedUrl('getObject', {
                        Bucket: BUCKET_NAME,
                        Key,
                        Expires: 86400 * 365 * 10
                    })
                    resolve({Key, url})
                }).catch(function(error){
                    reject(error)
                })
            })
        },
        deleteFromS3(Key){
            return new Promise(function(resolve, reject){
                s3Bucket.deleteObject({
                    Bucket: BUCKET_NAME,
                    Key
                }, function(error, data){
                    if(error){
                        reject(error)
                    }
                    resolve()
                })
            })
        }
    }
}